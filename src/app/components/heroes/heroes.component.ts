import { Component, OnInit } from '@angular/core';
import { HeroesService } from "../../services/heroes.service";

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styles: []
})
export class HeroesComponent implements OnInit {

  heroes:any;
  loading:boolean = false;

  constructor(private _heroesService:HeroesService) { 
    this._heroesService.getHeroes().subscribe(data => {
      console.log(data.data.results);
      
      this.loading=false;
      this.heroes=data.data.results;
      //setTimeout(() => {this.loading=false, this.heroes=data },3000 );
    })
  }

  ngOnInit() {
  }

  borrarHeroe(key$:string){
    this._heroesService.borrarHeroe(key$).subscribe (respuesta => {
      
      if (respuesta){
        console.log(respuesta);
      }
      else{
        // Eliminó correctamente
        delete this.heroes[key$];
      }
    });
  }

}
